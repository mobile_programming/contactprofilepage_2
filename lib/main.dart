import 'package:flutter/material.dart';

void main() {
  runApp(ContactProfilePage());
}

class ContactProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          brightness: Brightness.light,
          appBarTheme: AppBarTheme(
            color: Colors.pink,
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.indigo.shade500,
          )
      ),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),

        // body: Container(
        //   color: Colors.cyan[50],
        // ),

      ),
    );
  }

  Widget buildCallButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.call,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Call"),
      ],
    );
  }
  Widget buildTextButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.textsms_outlined,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Text"),
      ],
    );
  }

  Widget buildVideoCallButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.video_camera_front,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Video"),
      ],
    );
  }

  Widget buildEmailButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.email_outlined,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Email"),
      ],
    );
  }

  Widget buildDirectionsButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.directions,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Directions"),
      ],
    );
  }

  Widget buildPayButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.attach_money,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Pay"),
      ],
    );
  }
  Widget mobilePhoneListTile(){
    return ListTile(
      leading: Icon(Icons.call),
      title: Text("330-803-3390"),
      subtitle: Text("mobile"),
      trailing: IconButton(
        icon: Icon(Icons.message),
        color: Colors.indigo.shade500,
        onPressed: (){},
      ),
    );
  }
  Widget otherPhoneListTile(){
    return ListTile(
      leading: Text(" "),
      title: Text("440-440-3390"),
      subtitle: Text("other"),
      trailing: IconButton(
        icon: Icon(Icons.message),
        color: Colors.indigo.shade500,
        onPressed: (){},
      ),
    );
  }Widget emailListTile(){
    return ListTile(
      leading: Icon(Icons.email),
      title: Text("priyanka@priyanka.com"),
      subtitle: Text("work"),
    );
  }
  Widget addressListTile(){
    return ListTile(
      leading: Icon(Icons.location_on),
      title: Text("234 Sunset St, Burlingame"),
      subtitle: Text("home"),
      trailing: IconButton(
        icon: Icon(Icons.directions),
        color: Colors.indigo.shade500,
        onPressed: (){},
      ),
    );
  }
  AppBar buildAppBarWidget(){
    return AppBar(
      backgroundColor: Colors.cyan[200],
      leading: Icon(
        Icons.arrow_back,
        color: Colors.black,
      ),
      // title: Text("Hello Flutter App"),
      actions: <Widget>[
        IconButton(
            onPressed: (){},
            icon: Icon(
              Icons.star_border_outlined,
              color: Colors.black,
            )
        ),
      ],
    );
  }
  Widget buildBodyWidget(){
    return ListView (
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              // color: Colors.cyan,
              width: double.infinity,
              //Height constraint at Container widget level
              height: 250,
              child: Image.network(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.6435-9/209655741_2895752564012578_7336017457500908687_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=0dZ4PcwHljAAX9bZdri&_nc_ht=scontent.fbkk5-7.fna&oh=00_AfC2EJuvElg1qGGL-C3zQdtpOrVm5h-JnMAvraK3pV6idQ&oe=63CA037A",
                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(8.0),
                    child: Text("WIP",
                      style: TextStyle(fontSize: 30),
                    ),
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.blueGrey[600],
            ),

            Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  buildCallButton(),
                  buildTextButton(),
                  buildVideoCallButton(),
                  buildEmailButton(),
                  buildDirectionsButton(),
                  buildPayButton(),
                ],
              ),
            ),mobilePhoneListTile(),
            otherPhoneListTile(),
            emailListTile(),
            addressListTile(),
          ],
        ),
      ],
    );
  }
}
